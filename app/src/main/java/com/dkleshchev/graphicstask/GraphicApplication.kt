package com.dkleshchev.graphicstask

import android.app.Application
import com.dkleshchev.graphicstask.presentation.di.Scopes
import com.dkleshchev.graphicstask.presentation.di.module.AppModule
import com.dkleshchev.graphicstask.presentation.di.module.BitmapExportModule
import com.dkleshchev.graphicstask.presentation.di.module.BusinessLogicModule
import com.dkleshchev.graphicstask.presentation.di.module.NetworkModule
import toothpick.Scope
import toothpick.Toothpick
import toothpick.configuration.Configuration

class GraphicApplication : Application() {

    companion object {
        private const val endpoint = "https://demo.bankplus.ru"
    }

    private val appScope: Scope get() = Toothpick.openScope(Scopes.APP_SCOPE)

    override fun onCreate() {
        super.onCreate()

        initToothpick()
        initAppScope()
    }

    //only debug configuration for test task
    private fun initToothpick() {
        Toothpick.setConfiguration(Configuration.forDevelopment().preventMultipleRootScopes())
    }

    private fun initAppScope() {
        appScope.installModules(
                AppModule(this),
                NetworkModule(endpoint),
                BusinessLogicModule(),
                BitmapExportModule()
        )
    }

}
