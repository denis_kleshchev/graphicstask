package com.dkleshchev.graphicstask.presentation.main.result

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.dkleshchev.graphicstask.data.network.model.Point
import com.dkleshchev.graphicstask.presentation.common.BaseView
import com.github.mikephil.charting.data.Entry

interface ResultView : BaseView {

    fun showTable(points: List<Point>)

    fun showChart(entries: List<Entry>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showImageExportedMessage()

}
