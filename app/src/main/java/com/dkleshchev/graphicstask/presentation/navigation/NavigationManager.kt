package com.dkleshchev.graphicstask.presentation.navigation

import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class NavigationManager @Inject constructor() {

    private val localCiceroneMap: MutableMap<String, Cicerone<Router>> by lazy {
        HashMap<String, Cicerone<Router>>()
    }

    fun getNavigationHolder(tag: String): NavigatorHolder =
            getCicerone(tag).navigatorHolder

    fun getRouter(tag: String): Router =
            getCicerone(tag).router

    private fun getCicerone(tag: String): Cicerone<Router> {
        if (!localCiceroneMap.containsKey(tag)) {
            localCiceroneMap[tag] = Cicerone.create()
        }

        return localCiceroneMap.getValue(tag)
    }

}
