package com.dkleshchev.graphicstask.presentation.main.result

import android.graphics.Bitmap
import com.arellomobile.mvp.InjectViewState
import com.dkleshchev.graphicstask.domain.boundaries.BitmapSaver
import com.dkleshchev.graphicstask.domain.boundaries.PointsResultProvider
import com.dkleshchev.graphicstask.presentation.common.BasePresenter
import com.dkleshchev.graphicstask.presentation.navigation.NavigationManager
import javax.inject.Inject

@InjectViewState
class ResultPresenter @Inject constructor(
        private val resultProvider: PointsResultProvider,
        private val bitmapSaver: BitmapSaver,
        navigationManager: NavigationManager
) : BasePresenter<ResultView>(navigationManager) {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        showTable()
        showChart()
    }

    fun saveChart(bitmap: Bitmap) {
        logger.info("save chart! $bitmap")
        bitmapSaver.save(bitmap)
                .subscribe({ viewState.showImageExportedMessage() },
                        { logger.error("Error saving bitmap:", it) })
    }

    private fun showTable() {
        resultProvider.points
                .subscribe(viewState::showTable) { logger.warn("error listening result: $it") }
                .unsibscribeOnDestroy()
    }

    private fun showChart() {
        resultProvider.entries
                .subscribe(viewState::showChart) { logger.warn("error listening result: $it") }
                .unsibscribeOnDestroy()
    }

}
