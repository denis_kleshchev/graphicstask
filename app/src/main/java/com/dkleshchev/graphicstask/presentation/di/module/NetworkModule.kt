package com.dkleshchev.graphicstask.presentation.di.module

import com.dkleshchev.graphicstask.data.network.PointsProviderGatewayImplementation
import com.dkleshchev.graphicstask.data.network.PointsProviderGatewayProvider
import com.dkleshchev.graphicstask.data.network.api.BackendApi
import com.dkleshchev.graphicstask.data.network.api.BackendApiProvider
import com.dkleshchev.graphicstask.data.network.client.OkHttpClientProvider
import com.dkleshchev.graphicstask.domain.gateways.PointsProviderGateway
import com.dkleshchev.graphicstask.presentation.di.qualifier.Endpoint
import okhttp3.OkHttpClient
import toothpick.config.Module

class NetworkModule(endpoint: String) : Module() {

    init {
        bind(String::class.java).withName(Endpoint::class.java).toInstance(endpoint)
        bind(OkHttpClient::class.java).toProvider(OkHttpClientProvider::class.java).providesSingletonInScope()
        bind(BackendApi::class.java).toProvider(BackendApiProvider::class.java).providesSingletonInScope()

        bind(PointsProviderGatewayImplementation::class.java).singletonInScope()
        bind(PointsProviderGateway::class.java).toProvider(PointsProviderGatewayProvider::class.java).providesSingletonInScope()
    }

}