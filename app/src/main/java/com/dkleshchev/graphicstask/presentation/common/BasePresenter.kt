package com.dkleshchev.graphicstask.presentation.common

import com.arellomobile.mvp.MvpPresenter
import com.dkleshchev.graphicstask.presentation.main.MainPages
import com.dkleshchev.graphicstask.presentation.navigation.NavigationManager
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import ru.terrakok.cicerone.Router

abstract class BasePresenter<V : BaseView>(
        private val navigationManager: NavigationManager
) : MvpPresenter<V>() {

    private val compositeDisposable = CompositeDisposable()
    protected val logger: Logger = LoggerFactory.getLogger(javaClass.simpleName)

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    protected fun Disposable.unsibscribeOnDestroy() {
        compositeDisposable.add(this)
    }

    protected fun getRouter(tag: String) : Router {
        return navigationManager.getRouter(tag)
    }

    open fun back() {
        getRouter(MainPages.ROUTER).exit()
    }

}
