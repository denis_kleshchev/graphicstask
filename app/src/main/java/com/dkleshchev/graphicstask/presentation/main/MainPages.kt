package com.dkleshchev.graphicstask.presentation.main

object MainPages {

    const val ROUTER = "graphics task main activity router"
    const val INPUT = "graphics task input screen"
    const val RESULT = "graphics task result screen"

}
