package com.dkleshchev.graphicstask.presentation.main

import android.support.v4.app.FragmentManager
import com.dkleshchev.graphicstask.presentation.main.input.InputFragment
import com.dkleshchev.graphicstask.presentation.main.result.ResultFragment
import com.dkleshchev.graphicstask.presentation.navigation.AnimationDescriptor
import com.dkleshchev.graphicstask.presentation.navigation.FragmentNavigator
import com.dkleshchev.graphicstask.presentation.navigation.NavigationDelegate

class MainNavigator(
        delegate: NavigationDelegate,
        fragmentManager: FragmentManager,
        containerId: Int
) : FragmentNavigator(delegate, fragmentManager, containerId) {

    override fun create(screenKey: String, arguments: Any?): FragmentDescriptor {
        return when(screenKey) {
            MainPages.INPUT -> FragmentDescriptor(InputFragment.newInstance(), AnimationDescriptor.FADE)
            MainPages.RESULT -> FragmentDescriptor(ResultFragment.newInstance(), AnimationDescriptor.FROM_RIGHT)
            else -> throw RuntimeException("Wrong screen key!")
        }
    }

}
