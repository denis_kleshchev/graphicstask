package com.dkleshchev.graphicstask.presentation.di

object Scopes {

    const val APP_SCOPE = "graphicstask application scope"

}
