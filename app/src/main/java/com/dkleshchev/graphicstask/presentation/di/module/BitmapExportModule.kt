package com.dkleshchev.graphicstask.presentation.di.module

import com.dkleshchev.graphicstask.data.permissions.PermissionsGatewayImplementation
import com.dkleshchev.graphicstask.data.permissions.PermissionsGatewayProvider
import com.dkleshchev.graphicstask.data.storage.MediaStoreExporterImplementation
import com.dkleshchev.graphicstask.data.storage.MediaStoreExporterProvider
import com.dkleshchev.graphicstask.domain.boundaries.BitmapSaver
import com.dkleshchev.graphicstask.domain.gateways.MediaStoreExporter
import com.dkleshchev.graphicstask.domain.gateways.PermissionsGateway
import com.dkleshchev.graphicstask.domain.implementation.BitmapSaverImplementation
import com.dkleshchev.graphicstask.domain.provider.BitmapSaverProvider
import toothpick.config.Module

class BitmapExportModule : Module() {

    init {
        bind(PermissionsGatewayImplementation::class.java).singletonInScope()
        bind(PermissionsGateway::class.java).toProvider(PermissionsGatewayProvider::class.java).providesSingletonInScope()

        bind(MediaStoreExporterImplementation::class.java).singletonInScope()
        bind(MediaStoreExporter::class.java).toProvider(MediaStoreExporterProvider::class.java).providesSingletonInScope()

        bind(BitmapSaverImplementation::class.java).singletonInScope()
        bind(BitmapSaver::class.java).toProvider(BitmapSaverProvider::class.java).providesSingletonInScope()
    }

}
