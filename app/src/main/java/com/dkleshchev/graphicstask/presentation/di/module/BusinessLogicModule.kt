package com.dkleshchev.graphicstask.presentation.di.module

import com.dkleshchev.graphicstask.domain.boundaries.PointsRequestHandler
import com.dkleshchev.graphicstask.domain.boundaries.PointsResultProvider
import com.dkleshchev.graphicstask.domain.entities.PointsKeeper
import com.dkleshchev.graphicstask.domain.entities.PointsPublisher
import com.dkleshchev.graphicstask.domain.entities.PointsToEntitiesConverter
import com.dkleshchev.graphicstask.domain.implementation.PointsHolder
import com.dkleshchev.graphicstask.domain.implementation.PointsRequestInteractor
import com.dkleshchev.graphicstask.domain.implementation.PointsResultInteractor
import com.dkleshchev.graphicstask.domain.implementation.PointsToEntitiesConverterImplementation
import com.dkleshchev.graphicstask.domain.provider.*
import toothpick.config.Module

class BusinessLogicModule : Module() {

    init {
        bind(PointsHolder::class.java).singletonInScope()
        bind(PointsKeeper::class.java).toProvider(PointsKeeperProvider::class.java).providesSingletonInScope()
        bind(PointsPublisher::class.java).toProvider(PointsPublisherProvider::class.java).providesSingletonInScope()

        bind(PointsRequestInteractor::class.java).singletonInScope()
        bind(PointsRequestHandler::class.java).toProvider(PointsRequestHandlerProvider::class.java).providesSingletonInScope()

        bind(PointsResultInteractor::class.java).singletonInScope()
        bind(PointsResultProvider::class.java).toProvider(PointsResultProviderProvider::class.java).providesSingletonInScope()

        bind(PointsToEntitiesConverterImplementation::class.java).singletonInScope()
        bind(PointsToEntitiesConverter::class.java).toProvider(PointsToEntitiesConverterProvider::class.java).providesSingletonInScope()
    }

}
