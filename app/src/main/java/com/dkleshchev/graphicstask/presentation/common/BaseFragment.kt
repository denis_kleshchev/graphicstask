package com.dkleshchev.graphicstask.presentation.common

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.arellomobile.mvp.MvpAppCompatFragment
import com.dkleshchev.graphicstask.presentation.main.MainActivity
import toothpick.Scope
import toothpick.Toothpick

abstract class BaseFragment : MvpAppCompatFragment() {

    abstract val scope: Scope

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Toothpick.inject(this, scope)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            view ?: inflateView(inflater, container)

    protected val mainActivity: MainActivity get() = activity as MainActivity

    protected fun hideKeyboard() {
        val currentFocus: View? = mainActivity.currentFocus
        getInputMethodManager().hideSoftInputFromWindow(currentFocus?.windowToken, 0)
    }

    private fun getInputMethodManager(): InputMethodManager =
            mainActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    private fun inflateView(inflater: LayoutInflater, container: ViewGroup?): View? {
        val layoutIdAnnotation = javaClass.getAnnotation(LayoutId::class.java)
                ?: throw NotImplementedError("A fragment must be annotated with @LayoutId!")
        val layoutId = layoutIdAnnotation.layout
        return if (layoutId != DefaultLayoutId.LAYOUT_NOT_DEFINED) {
            inflater.inflate(layoutId, container, false)
        } else null
    }

}
