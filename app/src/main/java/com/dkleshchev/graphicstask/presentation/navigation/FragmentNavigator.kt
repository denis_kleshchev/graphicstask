package com.dkleshchev.graphicstask.presentation.navigation

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.text.TextUtils
import com.dkleshchev.graphicstask.presentation.common.BaseFragment
import ru.terrakok.cicerone.commands.Back
import ru.terrakok.cicerone.commands.BackTo
import ru.terrakok.cicerone.commands.Forward
import ru.terrakok.cicerone.commands.Replace

abstract class FragmentNavigator(private val delegate: NavigationDelegate,
                        private val fragmentManager: FragmentManager,
                        private val containerId: Int) : SupportNavigator() {

    protected abstract fun create(screenKey: String,
                                  arguments: Any?) : FragmentDescriptor

    override fun forward(forward: Forward) {
        val screenKey = forward.screenKey
        logger.info("forward $screenKey")

        performTransaction {
            create(screenKey, forward.transitionData)
                    .fill(it)
                    .addToBackStack(screenKey)
        }
    }

    override fun replace(replace: Replace) {
        val screenKey = replace.screenKey
        logger.info("forward $screenKey")

        if (fragmentManager.backStackEntryCount>0) {
            fragmentManager.popBackStackImmediate()
        }

        performTransaction {
            create(screenKey, replace.transitionData)
                    .fill(it)
                    .addToBackStack(screenKey)
        }
    }

    override fun back(back: Back) {
        logger.info("back $back")
        if (fragmentManager.backStackEntryCount > 1) {
            fragmentManager.executePendingTransactions()
            fragmentManager.popBackStackImmediate()
        } else {
            exit()
        }
    }

    override fun backTo(backTo: BackTo) {
        val screenKey = backTo.screenKey
        logger.info("BackTo: $screenKey")

        if(fragmentManager.backStackEntryCount > 0 &&
                TextUtils.equals(screenKey, fragmentManager.getBackStackEntryAt(fragmentManager.backStackEntryCount-1).name)) {
            return
        }

        if (screenKey == null) {
            backToRoot()
            return
        }

        for (i in 0 until fragmentManager.backStackEntryCount) {
            if (TextUtils.equals(screenKey, fragmentManager.getBackStackEntryAt(i).name)) {
                fragmentManager.popBackStackImmediate(screenKey, 0)
                return
            }
        }

        backToRoot()
    }

    private fun backToRoot() {
        for(i in 0..fragmentManager.backStackEntryCount) {
            fragmentManager.popBackStack()
        }
        fragmentManager.executePendingTransactions()
    }

    private fun performTransaction(action: (FragmentTransaction) -> Unit) {
        fragmentManager.beginTransaction().apply {
            action(this)
            commit()
        }
        fragmentManager.executePendingTransactions()
    }

    private fun exit() = delegate.exit()

    protected inner class FragmentDescriptor(private val fragment: BaseFragment,
                                             private val animation: AnimationDescriptor = AnimationDescriptor.EMPTY,
                                             private val tag: String? = null) {

        fun fill(fragmentTransaction: FragmentTransaction): FragmentTransaction {
            fragmentTransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit)
            return fragmentTransaction.replace(containerId, fragment, tag)
        }

    }

}
