package com.dkleshchev.graphicstask.presentation.main.input

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.dkleshchev.graphicstask.presentation.common.BaseView

interface InputView : BaseView {

    fun showProgress(isLoading: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)

}
