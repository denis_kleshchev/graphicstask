package com.dkleshchev.graphicstask.presentation.main.input

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.dkleshchev.graphicstask.R
import com.dkleshchev.graphicstask.presentation.common.BaseFragment
import com.dkleshchev.graphicstask.presentation.common.LayoutId
import com.dkleshchev.graphicstask.presentation.di.Scopes
import com.dkleshchev.graphicstask.presentation.navigation.BackListener
import kotlinx.android.synthetic.main.fragment_input.*
import toothpick.Scope
import toothpick.Toothpick

@LayoutId(layout = R.layout.fragment_input)
class InputFragment : BaseFragment(), InputView, BackListener {

    companion object {
        fun newInstance() = InputFragment()
    }

    @ProvidePresenter
    fun providePresenter(): InputPresenter {
        return scope.getInstance(InputPresenter::class.java)
    }

    @InjectPresenter
    lateinit var presenter: InputPresenter

    override val scope: Scope
        get() = Toothpick.openScopes(Scopes.APP_SCOPE)

    override fun showProgress(isLoading: Boolean) {
        progress.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    override fun showMessage(message: String) {
        AlertDialog.Builder(mainActivity)
                .setCancelable(true)
                .setTitle(message)
                .setNegativeButton(android.R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }
                .create()
                .show()
    }

    override fun onBack(): Boolean {
        presenter.back()
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        input_go_button.setOnClickListener {
            hideKeyboard()
            points_input.text.toString().toIntOrNull()?.let { presenter.requestPoints(it) }
        }
    }

}
