package com.dkleshchev.graphicstask.presentation.navigation

interface BackListener {

    fun onBack(): Boolean

}
