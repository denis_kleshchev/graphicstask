package com.dkleshchev.graphicstask.presentation.main.result

import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.dkleshchev.graphicstask.R
import com.dkleshchev.graphicstask.data.network.model.Point
import com.dkleshchev.graphicstask.presentation.common.BaseFragment
import com.dkleshchev.graphicstask.presentation.common.LayoutId
import com.dkleshchev.graphicstask.presentation.di.Scopes
import com.dkleshchev.graphicstask.presentation.navigation.BackListener
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import kotlinx.android.synthetic.main.fragment_result.*
import toothpick.Scope
import toothpick.Toothpick

@LayoutId(layout = R.layout.fragment_result)
class ResultFragment : BaseFragment(), ResultView, BackListener {

    companion object {
        fun newInstance() = ResultFragment()
    }

    @InjectPresenter
    lateinit var presenter: ResultPresenter

    @ProvidePresenter
    fun providePresenter(): ResultPresenter {
        return scope.getInstance(ResultPresenter::class.java)
    }

    private val adapter = ResultAdapter()

    override val scope: Scope
        get() = Toothpick.openScopes(Scopes.APP_SCOPE)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        result_recycler.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = this@ResultFragment.adapter
        }

        chart.apply {
            description.isEnabled = false
            legend.isEnabled = false
            isDragEnabled = true
            isScaleXEnabled = true
            isScaleYEnabled = false
            isDoubleTapToZoomEnabled = false
        }

        fab.setOnClickListener { saveChart() }
    }

    override fun showTable(points: List<Point>) {
        adapter.items = points
    }

    override fun showChart(entries: List<Entry>) {
        LineDataSet(entries, "").apply { mode = LineDataSet.Mode.CUBIC_BEZIER }
                .let { chart.data = LineData(it) }
        chart.invalidate()
    }

    override fun showImageExportedMessage() {
        mainActivity.showToast(getString(R.string.image_exported_message))
    }

    override fun onBack(): Boolean {
        presenter.back()
        return true
    }

    private fun saveChart() {
        chart.apply {
            val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            layout(0, 0, layoutParams.width, layoutParams.height)
            draw(canvas)
            presenter.saveChart(bitmap)
            requestLayout()
        }
    }

}
