package com.dkleshchev.graphicstask.presentation.navigation

interface NavigationDelegate {

    fun exit()

    fun showSystemMessage(message: String)

}
