package com.dkleshchev.graphicstask.presentation.main

import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.dkleshchev.graphicstask.R
import com.dkleshchev.graphicstask.presentation.di.Scopes
import com.dkleshchev.graphicstask.presentation.navigation.BackListener
import com.dkleshchev.graphicstask.presentation.navigation.NavigationDelegate
import com.dkleshchev.graphicstask.presentation.navigation.NavigationManager
import toothpick.Toothpick

class MainActivity : MvpAppCompatActivity(), NavigationDelegate, MainView {

    private val scope get() = Toothpick.openScopes(Scopes.APP_SCOPE)

    private val navigationManager: NavigationManager get() = scope.getInstance(NavigationManager::class.java)

    @InjectPresenter
    lateinit var presenter: MainPresenter

    @ProvidePresenter
    fun providePresenter(): MainPresenter {
        return scope.getInstance(MainPresenter::class.java)
    }

    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun exit() {
        finish()
    }

    override fun showSystemMessage(message: String) {
        showToast(message)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        initScopeAndInject()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        navigationManager.getNavigationHolder(MainPages.ROUTER)
                .setNavigator(MainNavigator(this, supportFragmentManager, R.id.container))
    }

    override fun onPause() {
        super.onPause()
        navigationManager.getNavigationHolder(MainPages.ROUTER).removeNavigator()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.findFragmentById(R.id.container)?.processBack() == true) {
            return
        }

        super.onBackPressed()
    }

    private fun initScopeAndInject() {
        Toothpick.inject(this, scope)
    }

    private fun Fragment.processBack(): Boolean {
        if (this is BackListener) {
            return onBack()
        }

        return false
    }

}
