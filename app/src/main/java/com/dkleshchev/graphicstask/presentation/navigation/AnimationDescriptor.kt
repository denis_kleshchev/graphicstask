package com.dkleshchev.graphicstask.presentation.navigation

import android.support.annotation.AnimRes
import com.dkleshchev.graphicstask.R

class AnimationDescriptor(@AnimRes val enter: Int,
                          @AnimRes val exit: Int,
                          @AnimRes val popEnter: Int = 0,
                          @AnimRes val popExit: Int = 0) {

    constructor() : this(0, 0)

    companion object {
        val EMPTY = AnimationDescriptor()
        val FADE = AnimationDescriptor(R.anim.fade_in, R.anim.fade_out)
        val FROM_RIGHT = AnimationDescriptor(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
    }

}
