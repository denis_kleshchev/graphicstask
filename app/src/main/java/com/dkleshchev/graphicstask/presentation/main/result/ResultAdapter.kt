package com.dkleshchev.graphicstask.presentation.main.result

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dkleshchev.graphicstask.R
import com.dkleshchev.graphicstask.data.network.model.Point
import kotlinx.android.synthetic.main.item_point.view.*
import kotlin.properties.Delegates

class ResultAdapter : RecyclerView.Adapter<ResultAdapter.ResultViewHolder>() {

    var items: List<Point> by Delegates.observable(emptyList()) { _, _, _ -> notifyDataSetChanged() }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultViewHolder {
        return ResultViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_point, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(viewHolder: ResultViewHolder, position: Int) {
        viewHolder.bind(items[position])
    }

    inner class ResultViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        @SuppressLint("SetTextI18n")
        fun bind(point: Point) {
            view.id_text.text = "${adapterPosition + 1}"
            view.x_text.text = "${point.x}"
            view.y_text.text = "${point.y}"
        }

    }

}
