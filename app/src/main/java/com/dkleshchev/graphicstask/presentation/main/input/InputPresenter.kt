package com.dkleshchev.graphicstask.presentation.main.input

import com.arellomobile.mvp.InjectViewState
import com.dkleshchev.graphicstask.domain.boundaries.PointsRequestHandler
import com.dkleshchev.graphicstask.presentation.common.BasePresenter
import com.dkleshchev.graphicstask.presentation.main.MainPages
import com.dkleshchev.graphicstask.presentation.navigation.NavigationManager
import javax.inject.Inject

@InjectViewState
class InputPresenter @Inject constructor(
        private val pointsRequestHandler: PointsRequestHandler,
        navigationManager: NavigationManager
) : BasePresenter<InputView>(navigationManager) {

    fun requestPoints(number: Int) {
        logger.info("Trying to request $number points")

        viewState.showProgress(true)
        pointsRequestHandler.request(number)
                .subscribe(
                        {
                            viewState.showProgress(false)
                            getRouter(MainPages.ROUTER).navigateTo(MainPages.RESULT)
                        },
                        { throwable ->
                            viewState.showProgress(false)
                            throwable.message?.let { viewState.showMessage(it) }
                        }
                )
                .unsibscribeOnDestroy()
    }

}
