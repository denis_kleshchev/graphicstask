package com.dkleshchev.graphicstask.presentation.di.module

import android.content.Context
import com.dkleshchev.graphicstask.presentation.navigation.NavigationManager
import toothpick.config.Module

class AppModule(context: Context) : Module() {

    init {
        bind(Context::class.java).toInstance(context)
        bind(NavigationManager::class.java).singletonInScope()
    }

}
