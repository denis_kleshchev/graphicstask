package com.dkleshchev.graphicstask.presentation.common

import com.arellomobile.mvp.MvpView

interface BaseView : MvpView
