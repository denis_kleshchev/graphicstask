package com.dkleshchev.graphicstask.presentation.di.qualifier

import javax.inject.Qualifier

@Qualifier
annotation class Endpoint
