package com.dkleshchev.graphicstask.presentation.navigation

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.commands.*
import kotlin.reflect.KClass

open class SupportNavigator : Navigator {

    protected val logger: Logger = LoggerFactory.getLogger(javaClass.simpleName)
    private val handlers: MutableMap<KClass<out Command>, (Command) -> Unit> = HashMap()

    init {
        handlers[Forward::class] = { command -> forward(command as Forward) }
        handlers[Replace::class] = { command -> replace(command as Replace) }
        handlers[BackTo::class] = { command -> backTo(command as BackTo) }
        handlers[Back::class] = { command -> back(command as Back) }
        handlers[SystemMessage::class] = { command -> systemMessage(command as SystemMessage) }
    }

    override fun applyCommand(command: Command) {
        handlers.getValue(command::class).invoke(command)
    }

    open fun forward(forward: Forward) {
        throw UnsupportedOperationException("forward not defined, override forward(Forward) method")
    }

    open fun replace(replace: Replace) {
        throw UnsupportedOperationException("replace not defined, override replace(Replace) method")
    }

    open fun backTo(backTo: BackTo) {
        throw UnsupportedOperationException("backTo not defined, override backTo(BackTo) method")
    }

    open fun back(back: Back) {
        throw UnsupportedOperationException("back not defined, override back(Back) method")
    }

    open fun systemMessage(systemMessage: SystemMessage) {
        throw UnsupportedOperationException("systemMessage not defined, override systemMessage(SystemMessage) method")
    }

}