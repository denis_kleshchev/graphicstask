package com.dkleshchev.graphicstask.presentation.main

import com.arellomobile.mvp.InjectViewState
import com.dkleshchev.graphicstask.presentation.common.BasePresenter
import com.dkleshchev.graphicstask.presentation.navigation.NavigationManager
import javax.inject.Inject

@InjectViewState
class MainPresenter @Inject constructor(
        navigationManager: NavigationManager
) : BasePresenter<MainView>(navigationManager) {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        getRouter(MainPages.ROUTER)
                .newRootScreen(MainPages.INPUT)
    }

}
