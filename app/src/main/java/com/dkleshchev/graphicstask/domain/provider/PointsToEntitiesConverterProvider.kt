package com.dkleshchev.graphicstask.domain.provider

import com.dkleshchev.graphicstask.domain.entities.PointsToEntitiesConverter
import com.dkleshchev.graphicstask.domain.implementation.PointsToEntitiesConverterImplementation
import javax.inject.Inject
import javax.inject.Provider

class PointsToEntitiesConverterProvider @Inject constructor(
        private val implementation: PointsToEntitiesConverterImplementation
) : Provider<PointsToEntitiesConverter> {

    override fun get(): PointsToEntitiesConverter {
        return implementation
    }

}
