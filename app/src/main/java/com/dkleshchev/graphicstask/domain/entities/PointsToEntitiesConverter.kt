package com.dkleshchev.graphicstask.domain.entities

import com.dkleshchev.graphicstask.data.network.model.Point
import com.github.mikephil.charting.data.Entry
import io.reactivex.Single

interface PointsToEntitiesConverter {

    fun convert(points: List<Point>): Single<List<Entry>>

}
