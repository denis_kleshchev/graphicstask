package com.dkleshchev.graphicstask.domain.implementation

import com.dkleshchev.graphicstask.domain.boundaries.PointsRequestHandler
import com.dkleshchev.graphicstask.domain.entities.PointsPublisher
import com.dkleshchev.graphicstask.domain.gateways.PointsProviderGateway
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class PointsRequestInteractor @Inject constructor(
        private val providerGateway: PointsProviderGateway,
        private val publisher: PointsPublisher
) : PointsRequestHandler {

    override fun request(number: Int): Completable {
        /*
        uncomment this and remove the string after block to set minimal visible loading time to 2 seconds
        return Single.zip(
                Observable.interval(2, TimeUnit.SECONDS).firstOrError(),
                providerGateway.loadPoints(number),
                BiFunction<Long, List<Point>, List<Point>> { _, points -> points }
        )*/
        return providerGateway.loadPoints(number)
                .flatMapCompletable { publisher.publish(it) }
                .observeOn(AndroidSchedulers.mainThread())
    }

}
