package com.dkleshchev.graphicstask.domain.boundaries

import com.dkleshchev.graphicstask.data.network.model.Point
import com.github.mikephil.charting.data.Entry
import io.reactivex.Single

interface PointsResultProvider {

    val points: Single<List<Point>>

    val entries: Single<List<Entry>>

}
