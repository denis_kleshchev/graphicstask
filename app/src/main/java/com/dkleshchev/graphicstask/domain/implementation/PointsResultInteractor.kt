package com.dkleshchev.graphicstask.domain.implementation

import com.dkleshchev.graphicstask.data.network.model.Point
import com.dkleshchev.graphicstask.domain.boundaries.PointsResultProvider
import com.dkleshchev.graphicstask.domain.entities.PointsKeeper
import com.dkleshchev.graphicstask.domain.entities.PointsToEntitiesConverter
import com.github.mikephil.charting.data.Entry
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class PointsResultInteractor @Inject constructor(
        private val keeper: PointsKeeper,
        private val converter: PointsToEntitiesConverter
) : PointsResultProvider{

    override val points: Single<List<Point>>
        get() = keeper.points.observeOn(AndroidSchedulers.mainThread())

    override val entries: Single<List<Entry>>
        get() = keeper.points
                .flatMap { converter.convert(it) }
                .observeOn(AndroidSchedulers.mainThread())

}
