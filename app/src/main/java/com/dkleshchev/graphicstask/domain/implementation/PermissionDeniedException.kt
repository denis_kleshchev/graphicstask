package com.dkleshchev.graphicstask.domain.implementation

class PermissionDeniedException : RuntimeException("Permission denied!")
