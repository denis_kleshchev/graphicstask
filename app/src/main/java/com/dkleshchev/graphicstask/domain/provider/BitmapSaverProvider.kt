package com.dkleshchev.graphicstask.domain.provider

import com.dkleshchev.graphicstask.domain.boundaries.BitmapSaver
import com.dkleshchev.graphicstask.domain.implementation.BitmapSaverImplementation
import javax.inject.Inject
import javax.inject.Provider

class BitmapSaverProvider @Inject constructor(
        private val implementation: BitmapSaverImplementation
) : Provider<BitmapSaver> {

    override fun get(): BitmapSaver {
        return implementation
    }

}
