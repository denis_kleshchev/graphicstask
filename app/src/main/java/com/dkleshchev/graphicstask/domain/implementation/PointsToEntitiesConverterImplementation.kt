package com.dkleshchev.graphicstask.domain.implementation

import com.dkleshchev.graphicstask.data.network.model.Point
import com.dkleshchev.graphicstask.domain.entities.PointsToEntitiesConverter
import com.github.mikephil.charting.data.Entry
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class PointsToEntitiesConverterImplementation @Inject constructor() : PointsToEntitiesConverter {

    override fun convert(points: List<Point>): Single<List<Entry>> {
        return Observable.fromIterable(points)
                .sorted { left, right -> left.x.compareTo(right.x) }
                .map { Entry(it.x.toFloat(), it.y.toFloat()) }
                .toList()
    }

}
