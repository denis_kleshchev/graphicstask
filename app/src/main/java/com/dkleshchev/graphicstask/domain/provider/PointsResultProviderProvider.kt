package com.dkleshchev.graphicstask.domain.provider

import com.dkleshchev.graphicstask.domain.boundaries.PointsResultProvider
import com.dkleshchev.graphicstask.domain.implementation.PointsResultInteractor
import javax.inject.Inject
import javax.inject.Provider

class PointsResultProviderProvider @Inject constructor(
        private val implementation: PointsResultInteractor
) : Provider<PointsResultProvider> {

    override fun get(): PointsResultProvider {
        return implementation
    }

}
