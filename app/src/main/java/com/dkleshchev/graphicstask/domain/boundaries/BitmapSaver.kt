package com.dkleshchev.graphicstask.domain.boundaries

import android.graphics.Bitmap
import io.reactivex.Completable

interface BitmapSaver {

    fun save(bitmap: Bitmap): Completable

}
