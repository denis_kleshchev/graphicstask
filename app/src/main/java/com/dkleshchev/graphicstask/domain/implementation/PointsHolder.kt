package com.dkleshchev.graphicstask.domain.implementation

import com.dkleshchev.graphicstask.data.network.model.Point
import com.dkleshchev.graphicstask.domain.entities.PointsKeeper
import com.dkleshchev.graphicstask.domain.entities.PointsPublisher
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import javax.inject.Inject

class PointsHolder @Inject constructor(): PointsKeeper, PointsPublisher {

    private val subject: Subject<List<Point>> = BehaviorSubject.createDefault(emptyList())

    override val points: Single<List<Point>>
        get() = subject.firstOrError()

    override fun publish(points: List<Point>): Completable = Completable.fromAction {
        subject.onNext(points)
    }

}
