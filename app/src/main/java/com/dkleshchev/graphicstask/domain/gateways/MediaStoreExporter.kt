package com.dkleshchev.graphicstask.domain.gateways

import android.graphics.Bitmap
import io.reactivex.Completable

interface MediaStoreExporter {

    fun export(bitmap: Bitmap) : Completable

}
