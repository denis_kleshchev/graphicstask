package com.dkleshchev.graphicstask.domain.provider

import com.dkleshchev.graphicstask.domain.boundaries.PointsRequestHandler
import com.dkleshchev.graphicstask.domain.implementation.PointsRequestInteractor
import javax.inject.Inject
import javax.inject.Provider

class PointsRequestHandlerProvider @Inject constructor(
        private val implementation: PointsRequestInteractor
) : Provider<PointsRequestHandler> {

    override fun get(): PointsRequestHandler {
        return implementation
    }

}
