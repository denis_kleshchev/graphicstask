package com.dkleshchev.graphicstask.domain.gateways

import com.dkleshchev.graphicstask.data.network.model.Point
import io.reactivex.Single

interface PointsProviderGateway {

    fun loadPoints(quantity: Int): Single<List<Point>>

}
