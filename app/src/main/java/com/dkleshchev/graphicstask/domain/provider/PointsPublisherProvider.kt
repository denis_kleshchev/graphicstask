package com.dkleshchev.graphicstask.domain.provider

import com.dkleshchev.graphicstask.domain.entities.PointsPublisher
import com.dkleshchev.graphicstask.domain.implementation.PointsHolder
import javax.inject.Inject
import javax.inject.Provider

class PointsPublisherProvider @Inject constructor(
        private val implementation: PointsHolder
) : Provider<PointsPublisher>{

    override fun get(): PointsPublisher {
        return implementation
    }

}
