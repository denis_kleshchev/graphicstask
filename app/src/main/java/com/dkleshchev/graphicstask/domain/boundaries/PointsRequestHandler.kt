package com.dkleshchev.graphicstask.domain.boundaries

import io.reactivex.Completable

interface PointsRequestHandler {

    fun request(number: Int): Completable

}
