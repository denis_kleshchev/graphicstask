package com.dkleshchev.graphicstask.domain.entities

import com.dkleshchev.graphicstask.data.network.model.Point
import io.reactivex.Completable

interface PointsPublisher {

    fun publish(points: List<Point>) : Completable

}
