package com.dkleshchev.graphicstask.domain.provider

import com.dkleshchev.graphicstask.domain.entities.PointsKeeper
import com.dkleshchev.graphicstask.domain.implementation.PointsHolder
import javax.inject.Inject
import javax.inject.Provider

class PointsKeeperProvider @Inject constructor(
        private val implementation: PointsHolder
) : Provider<PointsKeeper>{

    override fun get(): PointsKeeper {
        return implementation
    }

}
