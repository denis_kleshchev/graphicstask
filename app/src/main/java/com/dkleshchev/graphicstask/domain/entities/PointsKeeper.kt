package com.dkleshchev.graphicstask.domain.entities

import com.dkleshchev.graphicstask.data.network.model.Point
import io.reactivex.Single

interface PointsKeeper {

    val points: Single<List<Point>>

}
