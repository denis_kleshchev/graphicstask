package com.dkleshchev.graphicstask.domain.gateways

import io.reactivex.Single

interface PermissionsGateway {

    fun requestWriteExternalStoragePermission(): Single<Boolean>

}
