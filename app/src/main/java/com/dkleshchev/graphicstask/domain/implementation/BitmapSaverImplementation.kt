package com.dkleshchev.graphicstask.domain.implementation

import android.graphics.Bitmap
import com.dkleshchev.graphicstask.domain.boundaries.BitmapSaver
import com.dkleshchev.graphicstask.domain.gateways.MediaStoreExporter
import com.dkleshchev.graphicstask.domain.gateways.PermissionsGateway
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class BitmapSaverImplementation @Inject constructor(
        private val permissionsGateway: PermissionsGateway,
        private val mediaStoreExporter: MediaStoreExporter
) : BitmapSaver {

    override fun save(bitmap: Bitmap): Completable {
        return permissionsGateway.requestWriteExternalStoragePermission()
                .flatMapCompletable {
                    if (it) {
                        mediaStoreExporter.export(bitmap)
                    } else {
                        Completable.error(PermissionDeniedException())
                    }
                }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
    }

}
