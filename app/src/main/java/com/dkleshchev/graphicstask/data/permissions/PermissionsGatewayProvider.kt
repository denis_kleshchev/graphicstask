package com.dkleshchev.graphicstask.data.permissions

import com.dkleshchev.graphicstask.domain.gateways.PermissionsGateway
import javax.inject.Inject
import javax.inject.Provider

class PermissionsGatewayProvider @Inject constructor(
        private val implementation: PermissionsGatewayImplementation
) : Provider<PermissionsGateway> {

    override fun get(): PermissionsGateway {
        return implementation
    }

}
