package com.dkleshchev.graphicstask.data.storage

import android.content.Context
import android.graphics.Bitmap
import android.provider.MediaStore
import com.dkleshchev.graphicstask.domain.gateways.MediaStoreExporter
import io.reactivex.Completable
import javax.inject.Inject

class MediaStoreExporterImplementation @Inject constructor(
        private val context: Context
) : MediaStoreExporter {

    companion object {
        private const val title = "saved image title"
        private const val description = "saved image description"
    }

    override fun export(bitmap: Bitmap): Completable {
        return Completable.fromAction {
            MediaStore.Images.Media.insertImage(context.contentResolver, bitmap, title, description)
        }
    }

}
