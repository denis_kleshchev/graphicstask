package com.dkleshchev.graphicstask.data.permissions

import android.Manifest
import android.content.Context
import com.dkleshchev.graphicstask.domain.gateways.PermissionsGateway
import com.tedpark.tedpermission.rx2.TedRx2Permission
import io.reactivex.Single
import javax.inject.Inject

class PermissionsGatewayImplementation @Inject constructor(
        private val context: Context
) : PermissionsGateway {

    override fun requestWriteExternalStoragePermission(): Single<Boolean> {
        return TedRx2Permission.with(context)
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .request()
                .map { it.isGranted }
    }

}
