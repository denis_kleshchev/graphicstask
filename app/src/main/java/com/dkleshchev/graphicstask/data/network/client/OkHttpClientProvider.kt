package com.dkleshchev.graphicstask.data.network.client

import okhttp3.OkHttpClient
import javax.inject.Inject
import javax.inject.Provider

class OkHttpClientProvider @Inject constructor() : Provider<OkHttpClient> {

    override fun get(): OkHttpClient {
        return OkHttpClientBuilderFactory.unsafeClient.build()
    }

}
