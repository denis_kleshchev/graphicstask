package com.dkleshchev.graphicstask.data.network.api

import com.dkleshchev.graphicstask.data.network.jackson.JsonMapper
import com.dkleshchev.graphicstask.presentation.di.qualifier.Endpoint
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import javax.inject.Inject
import javax.inject.Provider

class BackendApiProvider @Inject constructor(
        @Endpoint private val endpoint: String,
        private val client: OkHttpClient
) : Provider<BackendApi> {

    override fun get(): BackendApi {
        return Retrofit.Builder()
                .baseUrl(endpoint)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(JacksonConverterFactory.create(JsonMapper()))
                .build()
                .create(BackendApi::class.java)
    }

}
