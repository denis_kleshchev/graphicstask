package com.dkleshchev.graphicstask.data.network.model

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

class PointsResponse @JsonCreator constructor(
        @JsonProperty(value = "result") var result: Int?,
        @JsonProperty(value = "points") var points: List<Point>?,
        @JsonProperty(value = "message") var message: String?
)
