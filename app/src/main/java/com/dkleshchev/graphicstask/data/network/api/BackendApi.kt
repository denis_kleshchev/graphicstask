package com.dkleshchev.graphicstask.data.network.api

import com.dkleshchev.graphicstask.data.network.model.Response
import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface BackendApi {

    @FormUrlEncoded
    @POST("mobws/json/pointsList")
    fun getPoints(@Field("count") pointsNumber: Int,
                  @Field("version") version: Double = 1.1): Single<Response>

}
