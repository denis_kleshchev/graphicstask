package com.dkleshchev.graphicstask.data.network

class BackendException (message: String?): RuntimeException(message)
