package com.dkleshchev.graphicstask.data.network.client

import android.annotation.SuppressLint
import android.content.Context
import android.support.annotation.RawRes
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.security.KeyStore
import java.security.SecureRandom
import java.security.cert.Certificate
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

object OkHttpClientBuilderFactory {

    private const val sslProtocolType = "SSL"
    private const val certificateFactoryType = "X.509"
    private const val alias = "ca"

    val unsafeClient: OkHttpClient.Builder
        get() {
            val x509TrustManager = object : X509TrustManager {
                @SuppressLint("TrustAllX509TrustManager")
                override fun checkClientTrusted(p0: Array<out X509Certificate>?, p1: String?) {

                }

                @SuppressLint("TrustAllX509TrustManager")
                override fun checkServerTrusted(p0: Array<out X509Certificate>?, p1: String?) {

                }

                override fun getAcceptedIssuers(): Array<X509Certificate> {
                    return arrayOf()
                }
            }

            val trustManagers: Array<out TrustManager> = arrayOf(x509TrustManager)

            return SSLContext.getInstance(sslProtocolType)
                    .apply {
                        init(null, trustManagers, SecureRandom())
                    }.socketFactory
                    .let { socketFactory ->
                        OkHttpClient.Builder().apply {
                            sslSocketFactory(socketFactory, trustManagers[0] as X509TrustManager)
                            hostnameVerifier { _, _ -> true }
                            addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                        }
                    }
        }

    fun Context.clientWithCertificate(@RawRes certificateId: Int): OkHttpClient.Builder {
        // Load CAs from an InputStream
        val certificateFactory = CertificateFactory.getInstance(certificateFactoryType)
        val certificate: Certificate = resources.openRawResource(certificateId).use {
            certificateFactory.generateCertificate(it)
        }

        // Create a KeyStore containing our trusted CAs
        return KeyStore.getInstance(KeyStore.getDefaultType()).run {
            load(null, null)
            setCertificateEntry(alias, certificate)

            // Create a TrustManager that trusts the CAs in our KeyStore.
            val tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm()
            val trustManagerFactory = TrustManagerFactory.getInstance(tmfAlgorithm)
            trustManagerFactory.init(this)
            trustManagerFactory.trustManagers[0] as X509TrustManager
        }
                .let { x509TrustManager ->
                    SSLContext.getInstance(sslProtocolType)
                            .apply {
                                init(null, arrayOf(x509TrustManager), SecureRandom())
                            }.socketFactory
                            .let { socketFactory ->
                                OkHttpClient.Builder().apply {
                                    sslSocketFactory(socketFactory, x509TrustManager)
                                    hostnameVerifier { _, _ -> true }
                                }
                            }
                }
    }

}
