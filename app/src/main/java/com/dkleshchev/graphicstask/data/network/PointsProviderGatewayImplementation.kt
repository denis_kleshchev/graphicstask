package com.dkleshchev.graphicstask.data.network

import com.dkleshchev.graphicstask.data.network.api.BackendApi
import com.dkleshchev.graphicstask.data.network.model.Point
import com.dkleshchev.graphicstask.data.network.model.PointsResponse
import com.dkleshchev.graphicstask.domain.gateways.PointsProviderGateway
import io.reactivex.Single
import javax.inject.Inject

class PointsProviderGatewayImplementation @Inject constructor(
        private val api: BackendApi
) : PointsProviderGateway {

    override fun loadPoints(quantity: Int): Single<List<Point>> {
        return api.getPoints(quantity)
                .flatMap { response ->
                    response.result?.let { Single.just(response.content) }
                            ?: Single.error<PointsResponse>(BackendException(response.content.message))
                }
                .map { it.points ?: emptyList() }
    }

}
