package com.dkleshchev.graphicstask.data.network.model

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

class Response @JsonCreator constructor(
        @JsonProperty(value = "result") var result: Int?,
        @JsonProperty(value = "response") var content: PointsResponse
)
