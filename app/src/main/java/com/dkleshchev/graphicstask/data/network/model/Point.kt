package com.dkleshchev.graphicstask.data.network.model

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

class Point @JsonCreator constructor(
        @JsonProperty(value = "x") var x: Double,
        @JsonProperty(value = "y") var y: Double
)
