package com.dkleshchev.graphicstask.data.network

import com.dkleshchev.graphicstask.domain.gateways.PointsProviderGateway
import javax.inject.Inject
import javax.inject.Provider

class PointsProviderGatewayProvider @Inject constructor(
        private val implementation: PointsProviderGatewayImplementation
) : Provider<PointsProviderGateway> {

    override fun get(): PointsProviderGateway {
        return implementation
    }

}
