package com.dkleshchev.graphicstask.data.storage

import com.dkleshchev.graphicstask.domain.gateways.MediaStoreExporter
import javax.inject.Inject
import javax.inject.Provider

class MediaStoreExporterProvider @Inject constructor(
        private val implementation: MediaStoreExporterImplementation
) : Provider<MediaStoreExporter> {

    override fun get(): MediaStoreExporter {
        return implementation
    }

}
