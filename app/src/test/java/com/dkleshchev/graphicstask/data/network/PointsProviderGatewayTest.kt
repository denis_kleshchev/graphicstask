package com.dkleshchev.graphicstask.data.network

import com.dkleshchev.graphicstask.data.network.api.BackendApi
import com.dkleshchev.graphicstask.data.network.model.Point
import com.dkleshchev.graphicstask.domain.gateways.PointsProviderGateway
import io.reactivex.observers.TestObserver
import org.junit.BeforeClass
import org.junit.ClassRule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class PointsProviderGatewayTest {

    companion object {

        @Mock
        private val backendApi: BackendApi = Mockito.mock(BackendApi::class.java)

        private lateinit var gateway: PointsProviderGateway

        @get:ClassRule
        @JvmStatic
        val backendRule: BackendApiRules = BackendApiRules(backendApi)

        @BeforeClass
        @JvmStatic
        fun setUp() {
            MockitoAnnotations.initMocks(this)
            gateway = PointsProviderGatewayImplementation(backendApi)
        }

    }

    @Test
    fun loadPointsNormally() {
        val testObserver = TestObserver<List<Point>>()

        gateway.loadPoints(10)
                .subscribe(testObserver)

        testObserver.run {
            awaitTerminalEvent()
            assertValue { it.size == 10 }
            assertComplete()
        }
    }

    @Test
    fun loadPointsRequest_0() {
        val testObserver = TestObserver<List<Point>>()

        gateway.loadPoints(0)
                .subscribe(testObserver)

        testObserver.run {
            awaitTerminalEvent()
            assertNoValues()
            assertError(BackendException::class.java)
            assertNotComplete()
        }
    }

    @Test
    fun loadPointsRequestOver_200() {
        val testObserver = TestObserver<List<Point>>()

        gateway.loadPoints(201)
                .subscribe(testObserver)

        testObserver.run {
            awaitTerminalEvent()
            assertNoValues()
            assertError(BackendException::class.java)
            assertNotComplete()
        }
    }

}
