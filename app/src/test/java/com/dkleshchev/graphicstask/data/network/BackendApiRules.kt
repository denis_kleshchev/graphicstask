package com.dkleshchev.graphicstask.data.network

import com.dkleshchev.graphicstask.data.network.api.BackendApi
import com.dkleshchev.graphicstask.data.network.model.Point
import com.dkleshchev.graphicstask.data.network.model.PointsResponse
import com.dkleshchev.graphicstask.data.network.model.Response
import io.reactivex.Single
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import org.mockito.Mockito.`when`

class BackendApiRules(
        private val backendApi: BackendApi
) : TestRule {

    companion object {

        private const val errorMessage = "Backend exception"
        private val errorPointsResponse = PointsResponse(-1, null, errorMessage)
        private val errorResponse = Response(null, errorPointsResponse)

        private val mockPoint = Point(0.0, 0.0)
        private val mockList = listOf(
                mockPoint,
                mockPoint,
                mockPoint,
                mockPoint,
                mockPoint,
                mockPoint,
                mockPoint,
                mockPoint,
                mockPoint,
                mockPoint
        )
        private val successfulResponse = Response(0, PointsResponse(null, mockList, null))

    }

    override fun apply(base: Statement, description: Description): Statement = object : Statement() {
        override fun evaluate() {
            `when`(backendApi.getPoints(0)).thenReturn(Single.just(errorResponse))
            `when`(backendApi.getPoints(201)).thenReturn(Single.just(errorResponse))
            `when`(backendApi.getPoints(10)).thenReturn(Single.just(successfulResponse))
            base.evaluate()
        }
    }

}
