package com.dkleshchev

import com.dkleshchev.graphicstask.data.network.PointsProviderGatewayTest
import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(PointsProviderGatewayTest::class)
class UnitTestSuite
